# ABCcursive
Police d'écriture cursive avec ligatures automatiques.

## Exemples :
![alt text](https://framagit.org/AlicVB/ABCcursive/raw/master/ex1.png "exemple 1")
![alt text](https://framagit.org/AlicVB/ABCcursive/raw/master/ex2.png "exemple 2")

## détails
Cette police est prévue pour "imiter" au maximum une jolie écriture "en attaché".

Elle gère *automatiquement* les différentes formes que doivent prendre les lettres en fonction de leur "voisines".

Exemple : le **i** n'a pas la même forme dans **oi** et **li**

## téléchargement
Lien vers la dernière version : [ABCcursive_gr.ttf](https://framagit.org/AlicVB/ABCcursive/raw/master/ABCcursive_gr.ttf)

## Logiciels
La police a été testée (et est intensivement utilisée) sous Linux avec [LibreOffice](https://fr.libreoffice.org/)

A priori, elle devrait aussi marcher sans problème sous Windows avec [LibreOffice](https://fr.libreoffice.org/) ou la suite Office (à confirmer)

## Licence
Cette police est fournie sous la licence "libre" [SIL](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web)

Ce qui signifie que vous êtes libre d'utiliser gratuitement cette police dans n'importe quel document.

Vous pouvez aussi modifier et/ou redistribuer cette police sous la même licence et en gardant une trace du copyright.

(c) A.RENAUDIN 2009-2016
